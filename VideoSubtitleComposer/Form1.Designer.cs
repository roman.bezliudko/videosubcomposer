﻿namespace VideoSubtitleComposer
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.StartTimeGroupBox = new System.Windows.Forms.GroupBox();
            this.EndTimeGroupBox = new System.Windows.Forms.GroupBox();
            this.OpenFileButton = new System.Windows.Forms.Button();
            this.FileNameLabel = new System.Windows.Forms.Label();
            this.StartHourLabel = new System.Windows.Forms.Label();
            this.StartMinuteLabel = new System.Windows.Forms.Label();
            this.StartSecondLabel = new System.Windows.Forms.Label();
            this.StartMillisecondLabel = new System.Windows.Forms.Label();
            this.EndMillisecondLabel = new System.Windows.Forms.Label();
            this.EndSecondLabel = new System.Windows.Forms.Label();
            this.EndMinuteLabel = new System.Windows.Forms.Label();
            this.EndHourLabel = new System.Windows.Forms.Label();
            this.AppendTextButton = new System.Windows.Forms.Button();
            this.FileContentRichTextBox = new System.Windows.Forms.RichTextBox();
            this.SubTextTextBox = new System.Windows.Forms.TextBox();
            this.SubFileOpenFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.SubTextNumberGroupBox = new System.Windows.Forms.GroupBox();
            this.SubTextNumberTextBox = new System.Windows.Forms.TextBox();
            this.SubTextGroupBox = new System.Windows.Forms.GroupBox();
            this.StartHourNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.StartMinuteNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.StartSecondNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.StartMillisecondNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.EndHourNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.EndMinuteNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.EndSecondNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.EndMillisecondNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.RefreshFileButton = new System.Windows.Forms.Button();
            this.StartTimeGroupBox.SuspendLayout();
            this.EndTimeGroupBox.SuspendLayout();
            this.SubTextNumberGroupBox.SuspendLayout();
            this.SubTextGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.StartHourNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartMinuteNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartSecondNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartMillisecondNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EndHourNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EndMinuteNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EndSecondNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EndMillisecondNumericUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // StartTimeGroupBox
            // 
            this.StartTimeGroupBox.Controls.Add(this.StartMillisecondNumericUpDown);
            this.StartTimeGroupBox.Controls.Add(this.StartSecondNumericUpDown);
            this.StartTimeGroupBox.Controls.Add(this.StartMinuteNumericUpDown);
            this.StartTimeGroupBox.Controls.Add(this.StartHourNumericUpDown);
            this.StartTimeGroupBox.Controls.Add(this.StartMillisecondLabel);
            this.StartTimeGroupBox.Controls.Add(this.StartSecondLabel);
            this.StartTimeGroupBox.Controls.Add(this.StartMinuteLabel);
            this.StartTimeGroupBox.Controls.Add(this.StartHourLabel);
            this.StartTimeGroupBox.Enabled = false;
            this.StartTimeGroupBox.Location = new System.Drawing.Point(120, 59);
            this.StartTimeGroupBox.Name = "StartTimeGroupBox";
            this.StartTimeGroupBox.Size = new System.Drawing.Size(188, 69);
            this.StartTimeGroupBox.TabIndex = 1;
            this.StartTimeGroupBox.TabStop = false;
            this.StartTimeGroupBox.Text = "Час початку";
            // 
            // EndTimeGroupBox
            // 
            this.EndTimeGroupBox.Controls.Add(this.EndMillisecondNumericUpDown);
            this.EndTimeGroupBox.Controls.Add(this.EndSecondNumericUpDown);
            this.EndTimeGroupBox.Controls.Add(this.EndMinuteNumericUpDown);
            this.EndTimeGroupBox.Controls.Add(this.EndHourNumericUpDown);
            this.EndTimeGroupBox.Controls.Add(this.EndMillisecondLabel);
            this.EndTimeGroupBox.Controls.Add(this.EndSecondLabel);
            this.EndTimeGroupBox.Controls.Add(this.EndMinuteLabel);
            this.EndTimeGroupBox.Controls.Add(this.EndHourLabel);
            this.EndTimeGroupBox.Enabled = false;
            this.EndTimeGroupBox.Location = new System.Drawing.Point(314, 59);
            this.EndTimeGroupBox.Name = "EndTimeGroupBox";
            this.EndTimeGroupBox.Size = new System.Drawing.Size(188, 69);
            this.EndTimeGroupBox.TabIndex = 2;
            this.EndTimeGroupBox.TabStop = false;
            this.EndTimeGroupBox.Text = "Час кінця";
            // 
            // OpenFileButton
            // 
            this.OpenFileButton.Location = new System.Drawing.Point(12, 15);
            this.OpenFileButton.Name = "OpenFileButton";
            this.OpenFileButton.Size = new System.Drawing.Size(112, 23);
            this.OpenFileButton.TabIndex = 3;
            this.OpenFileButton.Text = "Відкрити файл";
            this.OpenFileButton.UseVisualStyleBackColor = true;
            this.OpenFileButton.Click += new System.EventHandler(this.OpenFileButton_Click);
            // 
            // FileNameLabel
            // 
            this.FileNameLabel.AutoSize = true;
            this.FileNameLabel.Location = new System.Drawing.Point(145, 20);
            this.FileNameLabel.Name = "FileNameLabel";
            this.FileNameLabel.Size = new System.Drawing.Size(0, 13);
            this.FileNameLabel.TabIndex = 4;
            // 
            // StartHourLabel
            // 
            this.StartHourLabel.AutoSize = true;
            this.StartHourLabel.Location = new System.Drawing.Point(19, 42);
            this.StartHourLabel.Name = "StartHourLabel";
            this.StartHourLabel.Size = new System.Drawing.Size(27, 13);
            this.StartHourLabel.TabIndex = 5;
            this.StartHourLabel.Text = "год.";
            // 
            // StartMinuteLabel
            // 
            this.StartMinuteLabel.AutoSize = true;
            this.StartMinuteLabel.Location = new System.Drawing.Point(61, 42);
            this.StartMinuteLabel.Name = "StartMinuteLabel";
            this.StartMinuteLabel.Size = new System.Drawing.Size(21, 13);
            this.StartMinuteLabel.TabIndex = 7;
            this.StartMinuteLabel.Text = "хв.";
            // 
            // StartSecondLabel
            // 
            this.StartSecondLabel.AutoSize = true;
            this.StartSecondLabel.Location = new System.Drawing.Point(105, 42);
            this.StartSecondLabel.Name = "StartSecondLabel";
            this.StartSecondLabel.Size = new System.Drawing.Size(13, 13);
            this.StartSecondLabel.TabIndex = 8;
            this.StartSecondLabel.Text = "с";
            // 
            // StartMillisecondLabel
            // 
            this.StartMillisecondLabel.AutoSize = true;
            this.StartMillisecondLabel.Location = new System.Drawing.Point(145, 42);
            this.StartMillisecondLabel.Name = "StartMillisecondLabel";
            this.StartMillisecondLabel.Size = new System.Drawing.Size(21, 13);
            this.StartMillisecondLabel.TabIndex = 9;
            this.StartMillisecondLabel.Text = "мс";
            // 
            // EndMillisecondLabel
            // 
            this.EndMillisecondLabel.AutoSize = true;
            this.EndMillisecondLabel.Location = new System.Drawing.Point(145, 42);
            this.EndMillisecondLabel.Name = "EndMillisecondLabel";
            this.EndMillisecondLabel.Size = new System.Drawing.Size(21, 13);
            this.EndMillisecondLabel.TabIndex = 17;
            this.EndMillisecondLabel.Text = "мс";
            // 
            // EndSecondLabel
            // 
            this.EndSecondLabel.AutoSize = true;
            this.EndSecondLabel.Location = new System.Drawing.Point(105, 42);
            this.EndSecondLabel.Name = "EndSecondLabel";
            this.EndSecondLabel.Size = new System.Drawing.Size(13, 13);
            this.EndSecondLabel.TabIndex = 16;
            this.EndSecondLabel.Text = "с";
            // 
            // EndMinuteLabel
            // 
            this.EndMinuteLabel.AutoSize = true;
            this.EndMinuteLabel.Location = new System.Drawing.Point(61, 42);
            this.EndMinuteLabel.Name = "EndMinuteLabel";
            this.EndMinuteLabel.Size = new System.Drawing.Size(21, 13);
            this.EndMinuteLabel.TabIndex = 15;
            this.EndMinuteLabel.Text = "хв.";
            // 
            // EndHourLabel
            // 
            this.EndHourLabel.AutoSize = true;
            this.EndHourLabel.Location = new System.Drawing.Point(19, 42);
            this.EndHourLabel.Name = "EndHourLabel";
            this.EndHourLabel.Size = new System.Drawing.Size(27, 13);
            this.EndHourLabel.TabIndex = 11;
            this.EndHourLabel.Text = "год.";
            // 
            // AppendTextButton
            // 
            this.AppendTextButton.Enabled = false;
            this.AppendTextButton.Location = new System.Drawing.Point(12, 193);
            this.AppendTextButton.Name = "AppendTextButton";
            this.AppendTextButton.Size = new System.Drawing.Size(490, 23);
            this.AppendTextButton.TabIndex = 5;
            this.AppendTextButton.Text = "Додати";
            this.AppendTextButton.UseVisualStyleBackColor = true;
            this.AppendTextButton.Click += new System.EventHandler(this.AppendTextButton_Click);
            // 
            // FileContentRichTextBox
            // 
            this.FileContentRichTextBox.Enabled = false;
            this.FileContentRichTextBox.Location = new System.Drawing.Point(12, 222);
            this.FileContentRichTextBox.Name = "FileContentRichTextBox";
            this.FileContentRichTextBox.Size = new System.Drawing.Size(490, 177);
            this.FileContentRichTextBox.TabIndex = 6;
            this.FileContentRichTextBox.Text = "";
            // 
            // SubTextTextBox
            // 
            this.SubTextTextBox.Location = new System.Drawing.Point(12, 19);
            this.SubTextTextBox.Name = "SubTextTextBox";
            this.SubTextTextBox.Size = new System.Drawing.Size(462, 20);
            this.SubTextTextBox.TabIndex = 7;
            // 
            // SubFileOpenFileDialog
            // 
            this.SubFileOpenFileDialog.FileName = "openFileDialog1";
            // 
            // SubTextNumberGroupBox
            // 
            this.SubTextNumberGroupBox.Controls.Add(this.SubTextNumberTextBox);
            this.SubTextNumberGroupBox.Enabled = false;
            this.SubTextNumberGroupBox.Location = new System.Drawing.Point(15, 59);
            this.SubTextNumberGroupBox.Name = "SubTextNumberGroupBox";
            this.SubTextNumberGroupBox.Size = new System.Drawing.Size(99, 69);
            this.SubTextNumberGroupBox.TabIndex = 9;
            this.SubTextNumberGroupBox.TabStop = false;
            this.SubTextNumberGroupBox.Text = "№ субтитру";
            // 
            // SubTextNumberTextBox
            // 
            this.SubTextNumberTextBox.Location = new System.Drawing.Point(12, 26);
            this.SubTextNumberTextBox.Name = "SubTextNumberTextBox";
            this.SubTextNumberTextBox.Size = new System.Drawing.Size(49, 20);
            this.SubTextNumberTextBox.TabIndex = 10;
            this.SubTextNumberTextBox.Text = "1";
            // 
            // SubTextGroupBox
            // 
            this.SubTextGroupBox.Controls.Add(this.SubTextTextBox);
            this.SubTextGroupBox.Enabled = false;
            this.SubTextGroupBox.Location = new System.Drawing.Point(15, 134);
            this.SubTextGroupBox.Name = "SubTextGroupBox";
            this.SubTextGroupBox.Size = new System.Drawing.Size(487, 53);
            this.SubTextGroupBox.TabIndex = 10;
            this.SubTextGroupBox.TabStop = false;
            this.SubTextGroupBox.Text = "Текст";
            // 
            // StartHourNumericUpDown
            // 
            this.StartHourNumericUpDown.Location = new System.Drawing.Point(13, 19);
            this.StartHourNumericUpDown.Name = "StartHourNumericUpDown";
            this.StartHourNumericUpDown.Size = new System.Drawing.Size(36, 20);
            this.StartHourNumericUpDown.TabIndex = 11;
            // 
            // StartMinuteNumericUpDown
            // 
            this.StartMinuteNumericUpDown.Location = new System.Drawing.Point(55, 19);
            this.StartMinuteNumericUpDown.Name = "StartMinuteNumericUpDown";
            this.StartMinuteNumericUpDown.Size = new System.Drawing.Size(36, 20);
            this.StartMinuteNumericUpDown.TabIndex = 12;
            // 
            // StartSecondNumericUpDown
            // 
            this.StartSecondNumericUpDown.Location = new System.Drawing.Point(97, 19);
            this.StartSecondNumericUpDown.Name = "StartSecondNumericUpDown";
            this.StartSecondNumericUpDown.Size = new System.Drawing.Size(36, 20);
            this.StartSecondNumericUpDown.TabIndex = 13;
            // 
            // StartMillisecondNumericUpDown
            // 
            this.StartMillisecondNumericUpDown.Location = new System.Drawing.Point(139, 19);
            this.StartMillisecondNumericUpDown.Name = "StartMillisecondNumericUpDown";
            this.StartMillisecondNumericUpDown.Size = new System.Drawing.Size(36, 20);
            this.StartMillisecondNumericUpDown.TabIndex = 14;
            // 
            // EndHourNumericUpDown
            // 
            this.EndHourNumericUpDown.Location = new System.Drawing.Point(13, 19);
            this.EndHourNumericUpDown.Name = "EndHourNumericUpDown";
            this.EndHourNumericUpDown.Size = new System.Drawing.Size(36, 20);
            this.EndHourNumericUpDown.TabIndex = 18;
            // 
            // EndMinuteNumericUpDown
            // 
            this.EndMinuteNumericUpDown.Location = new System.Drawing.Point(55, 19);
            this.EndMinuteNumericUpDown.Name = "EndMinuteNumericUpDown";
            this.EndMinuteNumericUpDown.Size = new System.Drawing.Size(36, 20);
            this.EndMinuteNumericUpDown.TabIndex = 19;
            // 
            // EndSecondNumericUpDown
            // 
            this.EndSecondNumericUpDown.Location = new System.Drawing.Point(97, 19);
            this.EndSecondNumericUpDown.Name = "EndSecondNumericUpDown";
            this.EndSecondNumericUpDown.Size = new System.Drawing.Size(36, 20);
            this.EndSecondNumericUpDown.TabIndex = 20;
            // 
            // EndMillisecondNumericUpDown
            // 
            this.EndMillisecondNumericUpDown.Location = new System.Drawing.Point(139, 19);
            this.EndMillisecondNumericUpDown.Name = "EndMillisecondNumericUpDown";
            this.EndMillisecondNumericUpDown.Size = new System.Drawing.Size(36, 20);
            this.EndMillisecondNumericUpDown.TabIndex = 21;
            // 
            // RefreshFileButton
            // 
            this.RefreshFileButton.Location = new System.Drawing.Point(12, 407);
            this.RefreshFileButton.Name = "RefreshFileButton";
            this.RefreshFileButton.Size = new System.Drawing.Size(490, 23);
            this.RefreshFileButton.TabIndex = 11;
            this.RefreshFileButton.Text = "Оновити файл";
            this.RefreshFileButton.UseVisualStyleBackColor = true;
            this.RefreshFileButton.Click += new System.EventHandler(this.RefreshFileButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(523, 442);
            this.Controls.Add(this.RefreshFileButton);
            this.Controls.Add(this.SubTextGroupBox);
            this.Controls.Add(this.SubTextNumberGroupBox);
            this.Controls.Add(this.FileContentRichTextBox);
            this.Controls.Add(this.AppendTextButton);
            this.Controls.Add(this.FileNameLabel);
            this.Controls.Add(this.OpenFileButton);
            this.Controls.Add(this.EndTimeGroupBox);
            this.Controls.Add(this.StartTimeGroupBox);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "VideoSubComposer";
            this.StartTimeGroupBox.ResumeLayout(false);
            this.StartTimeGroupBox.PerformLayout();
            this.EndTimeGroupBox.ResumeLayout(false);
            this.EndTimeGroupBox.PerformLayout();
            this.SubTextNumberGroupBox.ResumeLayout(false);
            this.SubTextNumberGroupBox.PerformLayout();
            this.SubTextGroupBox.ResumeLayout(false);
            this.SubTextGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.StartHourNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartMinuteNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartSecondNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StartMillisecondNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EndHourNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EndMinuteNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EndSecondNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EndMillisecondNumericUpDown)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.GroupBox StartTimeGroupBox;
        private System.Windows.Forms.GroupBox EndTimeGroupBox;
        private System.Windows.Forms.Button OpenFileButton;
        private System.Windows.Forms.Label FileNameLabel;
        private System.Windows.Forms.Label StartHourLabel;
        private System.Windows.Forms.Label StartMinuteLabel;
        private System.Windows.Forms.Label StartSecondLabel;
        private System.Windows.Forms.Label StartMillisecondLabel;
        private System.Windows.Forms.Label EndMillisecondLabel;
        private System.Windows.Forms.Label EndSecondLabel;
        private System.Windows.Forms.Label EndMinuteLabel;
        private System.Windows.Forms.Label EndHourLabel;
        private System.Windows.Forms.Button AppendTextButton;
        private System.Windows.Forms.RichTextBox FileContentRichTextBox;
        private System.Windows.Forms.TextBox SubTextTextBox;
        private System.Windows.Forms.OpenFileDialog SubFileOpenFileDialog;
        private System.Windows.Forms.GroupBox SubTextNumberGroupBox;
        private System.Windows.Forms.TextBox SubTextNumberTextBox;
        private System.Windows.Forms.GroupBox SubTextGroupBox;
        private System.Windows.Forms.NumericUpDown StartHourNumericUpDown;
        private System.Windows.Forms.NumericUpDown StartMinuteNumericUpDown;
        private System.Windows.Forms.NumericUpDown StartSecondNumericUpDown;
        private System.Windows.Forms.NumericUpDown StartMillisecondNumericUpDown;
        private System.Windows.Forms.NumericUpDown EndHourNumericUpDown;
        private System.Windows.Forms.NumericUpDown EndMinuteNumericUpDown;
        private System.Windows.Forms.NumericUpDown EndSecondNumericUpDown;
        private System.Windows.Forms.NumericUpDown EndMillisecondNumericUpDown;
        private System.Windows.Forms.Button RefreshFileButton;
    }
}

