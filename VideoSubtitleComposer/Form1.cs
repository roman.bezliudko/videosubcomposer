﻿using System;
using System.IO;
using System.Windows.Forms;

namespace VideoSubtitleComposer
{
    public partial class Form1 : Form
    {
        private readonly string FILE_NOT_READ = "Не вдалося прочитати файл.";
        private readonly string INVALID_DATA_ENTERED = "Введено некоректні дані.";

        private string fileName;

        public Form1()
        {
            InitializeComponent();
        }

        private void OpenFileButton_Click(object sender, EventArgs e)
        {
            DialogResult result = SubFileOpenFileDialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                fileName = SubFileOpenFileDialog.FileName;
                try
                {
                    ReadFile();
                    FileNameLabel.Text = fileName;
                    EnableAllComponents();
                }
                catch (Exception)
                {
                    MessageBox.Show(FILE_NOT_READ);
                }
            }
        }

        private void ReadFile()
        {
            FileContentRichTextBox.Text = File.ReadAllText(fileName);
            FileContentRichTextBox.SelectionStart = FileContentRichTextBox.Text.Length;
            FileContentRichTextBox.ScrollToCaret();
        }

        private void EnableAllComponents()
        {
            SubTextNumberGroupBox.Enabled = true;
            StartTimeGroupBox.Enabled = true;
            EndTimeGroupBox.Enabled = true;
            SubTextGroupBox.Enabled = true;
            AppendTextButton.Enabled = true;
            FileContentRichTextBox.Enabled = true;
        }

        private void AppendTextButton_Click(object sender, EventArgs e)
        {
            try
            {
                string newSub = "\n\n";
                int subNo = Convert.ToInt32(SubTextNumberTextBox.Text);
                newSub += "" + subNo + '\n';
                int startHour = Convert.ToInt32(StartHourNumericUpDown.Value);
                int startMinute = Convert.ToInt32(StartMinuteNumericUpDown.Value);
                int startSecond = Convert.ToInt32(StartSecondNumericUpDown.Value);
                int startMillisecond = Convert.ToInt32(StartMillisecondNumericUpDown.Value);
                newSub += formatTimeValues(startHour, startMinute, startSecond, startMillisecond);
                newSub += " --> ";
                int endHour = Convert.ToInt32(EndHourNumericUpDown.Value);
                int endMinute = Convert.ToInt32(EndMinuteNumericUpDown.Value);
                int endSecond = Convert.ToInt32(EndSecondNumericUpDown.Value);
                int endMillisecond = Convert.ToInt32(EndMillisecondNumericUpDown.Value);
                newSub += formatTimeValues(endHour, endMinute, endSecond, endMillisecond);
                newSub += '\n' + SubTextTextBox.Text;
                File.AppendAllText(fileName, newSub);
                ReadFile();
                SuggestNextValues();
            }
            catch (Exception)
            {
                MessageBox.Show(INVALID_DATA_ENTERED);
            }
        }

        private string formatTimeValues(int hour, int minute, int second, int millisecond)
        {
            string result = "";
            result += (hour < 10) ? ("0" + hour) : ("" + hour);
            result += ":";
            result += (minute < 10) ? ("0" + minute) : ("" + minute);
            result += ":";
            result += (second < 10) ? ("0" + second) : ("" + second);
            result += ".";
            result += millisecond;
            return result;
        }

        private void SuggestNextValues()
        {
            SubTextNumberTextBox.Text = (Convert.ToInt32(SubTextNumberTextBox.Text) + 1).ToString();
            StartHourNumericUpDown.Value = EndHourNumericUpDown.Value;
            StartMinuteNumericUpDown.Value = EndMinuteNumericUpDown.Value;
            StartSecondNumericUpDown.Value = EndSecondNumericUpDown.Value;
            StartMillisecondNumericUpDown.Value = EndMillisecondNumericUpDown.Value;
            SubTextTextBox.Text = "";
        }

        private void RefreshFileButton_Click(object sender, EventArgs e)
        {
            File.WriteAllText(fileName, FileContentRichTextBox.Text);
            ReadFile();
        }
    }
}
